package com.example.testedatabinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.animation.ObjectAnimator;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import com.example.testedatabinding.databinding.ActivityLikesBinding;

public class LikesActivity extends AppCompatActivity {
    private ImageView imageView;
    private Button button;
    private LikesActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes);
        sincronizarComponentes();

        definirBinding();

        configurarOnclickButton();
    }

    private void definirBinding() {
        ActivityLikesBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_likes);
        viewModel = new ViewModelProvider(this).get(LikesActivityViewModel.class);
        viewModel.setContext(this);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
    }

    private void sincronizarComponentes() {
        button = findViewById(R.id.button);
        imageView = findViewById(R.id.imagem);
    }

    private void configurarOnclickButton() {

        button.setOnClickListener(view -> {
            viewModel.curtir(button);
        });
    }

}