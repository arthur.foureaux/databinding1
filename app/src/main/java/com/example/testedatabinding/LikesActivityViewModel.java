package com.example.testedatabinding;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.function.Function;

public class LikesActivityViewModel extends ViewModel {
    Context context;
    private MutableLiveData<String> nome = new MutableLiveData<>("Arthur");
    private MutableLiveData<Integer> imageResource = new MutableLiveData<>(R.drawable.imgapp);
    private MutableLiveData<String> sobreNome = new MutableLiveData<>("Foureaux");
    private MutableLiveData<Integer> likes = new MutableLiveData<>(0);

    public MutableLiveData<String> getNome() {
        return nome;
    }

    public MutableLiveData<String> getSobreNome() {
        return sobreNome;
    }

    public MutableLiveData<Integer> getLikes() {
        return likes;
    }

    public MutableLiveData<Integer> getImageResource() {
        return imageResource;
    }


    public void curtir(Button button){
        Integer nLikes = likes.getValue();
        nLikes++;

        if (nLikes > 10 && nLikes < 15){
            imageResource.setValue(R.drawable.imgapp2);
            button.setBackgroundColor(context.getResources().getColor(R.color.orange));
            likes.setValue(nLikes);
        } else if (nLikes >=15 && nLikes <= 20){
            imageResource.setValue(R.drawable.imgapp3);
            button.setBackgroundColor(context.getResources().getColor(R.color.blueNight));
            likes.setValue(nLikes);
        } else if (nLikes> 20){
            likes.setValue(0);
            button.setBackgroundColor(context.getResources().getColor(R.color.lilas));
            imageResource.setValue(R.drawable.imgapp);
        } else{
            likes.setValue(nLikes);
        }

    }

    public void setContext(Context context) {
        this.context = context;
    }
}
